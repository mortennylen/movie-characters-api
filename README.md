# Movie Character API

MovieCharacterAPI is a full CRUD RESTful Web API for movie characters, movies and franchises.
It is created with Spring Web, and it is exposing a relational PostgreSQL database which is created using Hibernate.

## Prerequisites

JDK 17 or newer.

## Entity models
### Character
```
    Id
    fullName
    alias 
    gender
    picture
    
    @ManyToMany (mappedBy = "characters")
    movies 
```
### Movie
```
    id
    movieTitle
    genre
    releaseYear
    director
    picture
    trailer
    
    @ManyToMany 
    characters 
    
    @ManyToOne
    franchise 
```

### Franchise
```
    id
    name
    description
    
    @OneToMany(mappedBy = "franchise")
    movies  
```

## Contact

Please make an issue with your request.

## License

..
