package no.noroff.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.services.Implementation.CharacterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/characters")

public class CharacterController {
    @Autowired
    private CharacterServiceImpl characterServiceImpl;

    // Get requests
    @Operation(summary = "Get all characters")
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterServiceImpl.getAllCharacters();
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    @Operation(summary = "Get a character by its id")
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable long id) {
        Character character = characterServiceImpl.getCharacterById(id);
        return new ResponseEntity<>(character, HttpStatus.OK);
    }

    @Operation(summary = "Get all movies with a character by its id")
    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getAllMoviesByCharacter(@PathVariable long id) {
        Set<Movie> movies = characterServiceImpl.getAllMoviesByCharacter(id);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    // Post requests
    @Operation(summary = "Add a new character")
    @PostMapping
    public ResponseEntity<Character> addNewCharacter(@Valid @RequestBody Character character) {
        return new ResponseEntity<>(characterServiceImpl.addNewCharacter(character), HttpStatus.OK);
    }

    // Put requests
    @Operation(summary = "Update a character by its id")
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@Valid @PathVariable long id, @Valid @RequestBody Character character) {
        return new ResponseEntity<>(characterServiceImpl.updateCharacter(id, character), HttpStatus.OK);
    }

    // Patch requests
    @Operation(summary = "Partially update a character by its id")
    @PatchMapping("/{id}")
    public ResponseEntity<Character> updateMoviePartial(@PathVariable long id, @RequestBody Character character) {
        return new ResponseEntity<>(characterServiceImpl.updateCharacterPartial(id, character),HttpStatus.OK);
    }

    // Delete requests
    @Operation(summary = "Delete a character by its id")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCharacter(@PathVariable Long id) {
        characterServiceImpl.deleteCharacter(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
