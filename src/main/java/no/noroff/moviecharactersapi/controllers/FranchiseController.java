package no.noroff.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.MoviesInFranchiseDto;
import no.noroff.moviecharactersapi.services.Implementation.FranchiseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/api/franchises")

public class FranchiseController {

    @Autowired
    private FranchiseServiceImpl franchiseServiceImpl;


    // Get requests
    @Operation(summary = "Get all franchises")
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseServiceImpl.getAllFranchises();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }
    @Operation(summary = "Get a franchise by its id")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable long id) {
        Franchise franchise = franchiseServiceImpl.getFranchiseById(id);
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }

    @Operation(summary = "Get all movies in a franchise by its id")
    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesByFranchise(@PathVariable long id) {
        Set<Movie> movies = franchiseServiceImpl.getAllMoviesByFranchise(id);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @Operation(summary = "Get all characters in a franchise by its id")
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getAllCharactersByFranchise(@PathVariable long id) {
        Set<Character> characters = franchiseServiceImpl.getAllCharactersByFranchise(id);
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    // Post requests
    @Operation(summary = "Add a new franchise")
    @PostMapping
    public ResponseEntity<Franchise> addNewFranchise(@Valid @RequestBody Franchise franchise){
        return new ResponseEntity<>(franchiseServiceImpl.addNewFranchise(franchise), HttpStatus.OK);
    }

    // Put requests
    @Operation(summary = "Update a franchise by its id")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@Valid @RequestBody Franchise franchise, @PathVariable long id){
        return new ResponseEntity<>(franchiseServiceImpl.updateFranchise(franchise, id), HttpStatus.OK);
    }

    @Operation(summary = "Update movies in a franchise by its id")
    @PutMapping("/{id}/movies")
    public ResponseEntity<Franchise> updateMoviesInFranchise(@PathVariable long id, @RequestBody MoviesInFranchiseDto moviesInFranchiseDto) {
        return new ResponseEntity<>(franchiseServiceImpl.updateMoviesInFranchise(id, moviesInFranchiseDto), HttpStatus.OK);
    }

    // Patch requests
    @Operation(summary = "Partially update a franchise by its id")
    @PatchMapping("/{id}")
    public ResponseEntity<Franchise> updateMoviePartial(@PathVariable long id, @RequestBody Franchise franchise) {
        return new ResponseEntity<>(franchiseServiceImpl.updateFranchisePartial(id, franchise),HttpStatus.OK);
    }

    // Delete requests
    @Operation(summary = "Delete a franchise by its id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable long id) {
        franchiseServiceImpl.deleteFranchise(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
