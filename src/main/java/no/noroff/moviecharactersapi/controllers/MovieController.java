package no.noroff.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.CharactersInMovieDto;
import no.noroff.moviecharactersapi.services.Implementation.MovieServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/movies")
public class MovieController {
    @Autowired
    MovieServiceImpl movieServiceImpl;

    // Get requests
    @Operation(summary = "Get all movies")
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return new ResponseEntity<>(movieServiceImpl.getAllMovies(), HttpStatus.OK);
    }

    @Operation(summary = "Get a movie by its id")
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable long id) {
        return new ResponseEntity<>(movieServiceImpl.getMovieById(id),HttpStatus.OK);
    }

    @Operation(summary = "Get all characters in a movie by its id")
    @GetMapping("/{id}/characters")
    public  ResponseEntity<Set<Character>> getCharactersByMovie(@PathVariable long id) {
        return new ResponseEntity<>(movieServiceImpl.getCharactersByMovie(id),HttpStatus.OK);
    }

    // Post requests
    @Operation(summary = "Add a new movie")
    @PostMapping
    public ResponseEntity<Movie> addNewMovie(@Valid @RequestBody Movie movie) {
        return new ResponseEntity<>(movieServiceImpl.addNewMovie(movie), HttpStatus.CREATED);
    }

    // Put requests
    @Operation(summary = "Update a movie by its id")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable long id, @Valid @RequestBody Movie movie) {
        return new ResponseEntity<>(movieServiceImpl.updateMovie(id,movie),HttpStatus.OK);
    }

    @Operation(summary = "Update characters in a movie by its id")
    @PutMapping ("/{id}/characters")
    public ResponseEntity<Movie> updateCharactersInMovie (@PathVariable long id, @RequestBody CharactersInMovieDto charactersInMovieDto) {
        return new ResponseEntity<>(movieServiceImpl.updateCharactersInMovie(id, charactersInMovieDto), HttpStatus.OK);
    }

    // Patch requests
    @Operation(summary = "Partially update a movie by its id")
    @PatchMapping("/{id}")
    public ResponseEntity<Movie> updateMoviePartial(@PathVariable long id, @RequestBody Movie movie) {
        return new ResponseEntity<>(movieServiceImpl.updateMoviePartial(id,movie),HttpStatus.OK);
    }

    // Delete requests
    @Operation(summary = "Delete a movie by its id")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable long id) {
        movieServiceImpl.deleteMovie(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }







}
