package no.noroff.moviecharactersapi.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharactersInMovieDto {
    private long[] characters;
}
