package no.noroff.moviecharactersapi.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoviesInFranchiseDto {
    private long[] movies;
}
