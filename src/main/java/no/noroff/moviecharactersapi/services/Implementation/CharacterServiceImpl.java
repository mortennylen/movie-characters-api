package no.noroff.moviecharactersapi.services.Implementation;

import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.repositories.CharacterRepository;
import no.noroff.moviecharactersapi.services.Interfaces.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;

@Service
public class CharacterServiceImpl implements CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    // Create
    public Character addNewCharacter(Character character) {
        character = characterRepository.save(character);
        return character;
    }

    // Read
    public List<Character> getAllCharacters() {
        return characterRepository.findAll();
    }

    public Character getCharacterById(long id) {
        return characterRepository.findById(id).orElse(null);
    }

    public Set<Movie> getAllMoviesByCharacter(long characterId) {
        return characterRepository.findById(characterId).map(Character::getMovies)
                .orElseThrow(() -> new ExpressionException("No movie with this character found"));
    }

    // Update
    public Character updateCharacter(long id, Character updatedCharacter) {
        Character character = characterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Character not found for this id: " + id));

        character.setFullName(updatedCharacter.getFullName());
        character.setAlias(updatedCharacter.getAlias());
        character.setGender(updatedCharacter.getGender());
        character.setPicture(updatedCharacter.getPicture());
        character.setMovies(updatedCharacter.getMovies());

        return characterRepository.save(character);
    }

    public Character updateCharacterPartial(long id, Character characterDetails) {
        Character character = characterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));

        if (characterDetails.getFullName() != null) {
            character.setFullName(characterDetails.getFullName());
        }
        if (characterDetails.getAlias() != null) {
            character.setAlias(characterDetails.getAlias());
        }
        if (characterDetails.getGender() != null) {
            character.setGender(characterDetails.getGender());
        }
        if (characterDetails.getPicture() != null) {
            character.setPicture(characterDetails.getPicture());
        }
        if (characterDetails.getMovies() != null) {
            character.setMovies(characterDetails.getMovies());
        }

        return characterRepository.save(character);
    }

    // Delete
    public void deleteCharacter(long characterId) {
        if (!characterRepository.existsById(characterId)) {
            throw new ExpressionException("No character with this id was found " + characterId);
        }
        characterRepository.deleteById(characterId);
    }
}
