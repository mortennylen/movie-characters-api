package no.noroff.moviecharactersapi.services.Implementation;
import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.MoviesInFranchiseDto;
import no.noroff.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.moviecharactersapi.repositories.MovieRepository;
import no.noroff.moviecharactersapi.services.Interfaces.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    // Create
    public Franchise addNewFranchise(Franchise franchise) {
        franchiseRepository.save(franchise);
        return franchise;
    }

    // Read
    public List<Franchise> getAllFranchises() {
        return franchiseRepository.findAll();
    }

    public Franchise getFranchiseById(long id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if (franchise.isEmpty()) {
            throw new ExpressionException("No Franchise was found with this id " + id);
        } else {
            return franchise.get();
        }
    }

    public Set<Movie> getAllMoviesByFranchise(long id) {
        if (franchiseRepository.existsById(id)) {
            Optional<Franchise> franchise = franchiseRepository.findById(id);
            return franchise.get().getMovies();
        } else {
            throw new ExpressionException("No Franchise was found with this id " + id);
        }
    }

    public Set<Character> getAllCharactersByFranchise(long id) {
        if (franchiseRepository.existsById(id)) {
            Set<Character> list = new HashSet<>();
            for (Movie m : franchiseRepository.findById(id).get().getMovies()) {
                list.addAll(m.getCharacters());
            }
            return list;
        } else {
            throw new ExpressionException("No Franchise was found with this id " + id);
        }
    }

    // Update
    public Franchise updateFranchise(Franchise updatedFranchise, long id) {

        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("No Franchise was found with this id" + id));

        franchise.setName(updatedFranchise.getName());
        franchise.setDescription(updatedFranchise.getDescription());
        franchise.setMovies(updatedFranchise.getMovies());

        franchiseRepository.save(franchise);

        return franchise;
    }

    public Franchise updateFranchisePartial(long id, Franchise franchiseDetails) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Franchise not found for this id: " + id));

        if (franchiseDetails.getName() != null) {
            franchise.setName(franchiseDetails.getName());
        }
        if (franchiseDetails.getDescription() != null) {
            franchise.setDescription(franchiseDetails.getDescription());
        }
        if (franchiseDetails.getMovies() != null) {
            franchise.setMovies(franchiseDetails.getMovies());

        }
        franchiseRepository.save(franchise);
        return franchise;

    }

    public Franchise updateMoviesInFranchise (long id, MoviesInFranchiseDto moviesInFranchiseDto) {
        // Stores movies array from franchiseDto
        long[] addedMoviesId = moviesInFranchiseDto.getMovies();

        // Finds the franchise corresponding to the id
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Franchise not found for this id: " + id));

        // Remove existing movies in franchise
        for (Movie movie: franchise.getMovies()) {
            movie.setFranchise(null);
        }

        // Find the movie from their ids and add the franchise
        for (long movieId: addedMoviesId) {
            movieRepository.findById(movieId).ifPresent(movie -> movie.setFranchise(franchise));
        }

        return franchiseRepository.save(franchise);
    }

    // Delete
    public void deleteFranchise(long id) {

        Franchise franchise = franchiseRepository.findById(id)
                .orElseThrow(() -> new ExpressionException("No Franchise was found with this id " + id));

        for (Movie movie : franchise.getMovies()) {
            movie.setFranchise(null);
        }
        franchiseRepository.delete(franchise);
    }
}
