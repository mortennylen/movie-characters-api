package no.noroff.moviecharactersapi.services.Implementation;

import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.CharactersInMovieDto;
import no.noroff.moviecharactersapi.repositories.CharacterRepository;
import no.noroff.moviecharactersapi.repositories.MovieRepository;
import no.noroff.moviecharactersapi.services.Interfaces.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    MovieRepository movieRepository;
    @Autowired
    CharacterRepository characterRepository;

    // Create
    public Movie addNewMovie(Movie movie) {
        return movieRepository.save(movie);
    }

    // Read
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public Movie getMovieById(long id) {
       return movieRepository.findById(id).orElse(null);
    }

    public Set<Character> getCharactersByMovie(long id) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));

        return movie.getCharacters();
    }

    // Update
    public Movie updateMovie(long id, Movie updatedMovie) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));

        movie.setMovieTitle(updatedMovie.getMovieTitle());
        movie.setGenre(updatedMovie.getGenre());
        movie.setReleaseYear(updatedMovie.getReleaseYear());
        movie.setDirector(updatedMovie.getDirector());
        movie.setPicture(updatedMovie.getPicture());
        movie.setTrailer(updatedMovie.getTrailer());
        movie.setFranchise(updatedMovie.getFranchise());

        return movieRepository.save(movie);
    }

    public Movie updateMoviePartial(long id, Movie movieDetails) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));

        if (movieDetails.getMovieTitle() != null) {
            movie.setMovieTitle(movieDetails.getMovieTitle());
        }
        if (movieDetails.getGenre() != null) {
            movie.setGenre(movieDetails.getGenre());
        }
        if (movieDetails.getReleaseYear() != null) {
            movie.setReleaseYear(movieDetails.getReleaseYear());
        }
        if (movieDetails.getDirector() != null) {
            movie.setDirector(movieDetails.getDirector());
        }
        if (movieDetails.getPicture() != null) {
            movie.setPicture(movieDetails.getPicture());
        }
        if (movieDetails.getTrailer() != null) {
            movie.setTrailer(movieDetails.getTrailer());
        }
        if (movieDetails.getFranchise() != null) {
            movie.setFranchise(movieDetails.getFranchise());
        }

        return movieRepository.save(movie);
    }

    public Movie updateCharactersInMovie (long id, CharactersInMovieDto charactersInMovieDto) {
        // Stores characters array from movieDto
        long[] addedCharactersId = charactersInMovieDto.getCharacters();

        // Finds the franchise corresponding to the id
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));

        Set addedCharactersSet = new HashSet();

        // Finds characters by id and add them to the addedCharactersSet
        for (long characterId: addedCharactersId) {
            characterRepository.findById(characterId).ifPresent(character -> addedCharactersSet.add(character));
        }

        movie.setCharacters(addedCharactersSet);

        return movieRepository.save(movie);
    }

    // Delete
    public void deleteMovie(long id) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Movie not found for this id: " + id));
        movieRepository.delete(movie);
    }





}
