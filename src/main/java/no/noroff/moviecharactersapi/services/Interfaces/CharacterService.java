package no.noroff.moviecharactersapi.services.Interfaces;

import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;

import java.util.List;
import java.util.Set;

public interface CharacterService {
    Character addNewCharacter(Character character);
    List<Character> getAllCharacters();
    Character getCharacterById(long id);
    Set<Movie> getAllMoviesByCharacter(long characterId);
    Character updateCharacter(long id, Character updatedCharacter);
    Character updateCharacterPartial(long id, Character characterDetails);
    void deleteCharacter(long characterId);
}
