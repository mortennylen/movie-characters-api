package no.noroff.moviecharactersapi.services.Interfaces;

import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Franchise;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.MoviesInFranchiseDto;

import java.util.List;
import java.util.Set;

public interface FranchiseService {
    Franchise addNewFranchise(Franchise franchise);
    List<Franchise> getAllFranchises();
    Franchise getFranchiseById(long id);
    Set<Movie> getAllMoviesByFranchise(long id);
    Set<Character> getAllCharactersByFranchise(long id);
    Franchise updateFranchise(Franchise updatedFranchise, long id);
    Franchise updateFranchisePartial(long id, Franchise franchiseDetails);
    Franchise updateMoviesInFranchise (long id, MoviesInFranchiseDto moviesInFranchiseDto);
    void deleteFranchise(long id);

}
