package no.noroff.moviecharactersapi.services.Interfaces;

import no.noroff.moviecharactersapi.models.domain.Character;
import no.noroff.moviecharactersapi.models.domain.Movie;
import no.noroff.moviecharactersapi.models.dto.CharactersInMovieDto;

import java.util.List;
import java.util.Set;

public interface MovieService {
    Movie addNewMovie(Movie movie);
    List<Movie> getAllMovies();
    Movie getMovieById(long id);
    Set<Character> getCharactersByMovie(long id);
    Movie updateMovie(long id, Movie updatedMovie);
    Movie updateMoviePartial(long id, Movie movieDetails);
    Movie updateCharactersInMovie (long id, CharactersInMovieDto charactersInMovieDto);
    void deleteMovie(long id);
}
